# Snapsys
Scripts to take, manipulate and transfer BTRFS snapshots. The typical
use is to take snapshots of the local machine and transfer them to a
specific USB stick when needed, or to a remote machine.
