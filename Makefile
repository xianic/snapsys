PREFIX ?= /usr
PREFIX_SUBST ?= $(PREFIX)
SYSCONF ?= /etc
SYSCONF_SUBST ?= $(SYSCONF)
OUTDIR := out
SRCDIR := src
DESTDIR ?= /

PREFIX_SRC := $(shell cd $(SRCDIR)/prefix && find -type f -name '*.in')
PREFIX_CPY := $(shell cd $(SRCDIR)/prefix && find -type f ! -name '*.in')
SYSCONF_SRC := $(shell cd $(SRCDIR)/sysconf && find -type f -name '*.in')
SYSCONF_CPY := $(shell cd $(SRCDIR)/sysconf && find -type f ! -name '*.in')

.PHONY: all install clean
all:

define src_cpy
$(OUTDIR)/$(3)/$(1): $(SRCDIR)/$(2)/$(1)
	mkdir -p "$$(dir $$@)"
	cp -p "$$(^)" "$$@"
all: $(OUTDIR)/$(3)/$(1)
endef

define src_sed
$(OUTDIR)$(3)/$(1:.in=): $(SRCDIR)/$(2)/$(1)
	mkdir -p "$$(dir $$@)"
	sed \
		-e 's:@PREFIX@:$(PREFIX_SUBST):g' \
		-e 's:@SYSCONF@:$(SYSCONF_SUBST):g' \
		< "$$(^)" \
		> "$$@"
	chmod --reference="$$(^)" "$$@"
all: $(OUTDIR)$(3)/$(1:.in=)
endef

$(foreach sf,$(PREFIX_CPY),$(eval $(call src_cpy,$(sf),prefix,$(PREFIX))))
$(foreach sf,$(PREFIX_SRC),$(eval $(call src_sed,$(sf),prefix,$(PREFIX))))
$(foreach sf,$(SYSCONF_CPY),$(eval $(call src_cpy,$(sf),sysconf,$(SYSCONF))))
$(foreach sf,$(SYSCONF_SRC),$(eval $(call src_sed,$(sf),sysconf,$(SYSCONF))))

install: all
	[ -n "$(DESTDIR)" ] && mkdir -p $(DESTDIR)
	cp -Rt "$(DESTDIR)" $(OUTDIR)/*

clean: debclean
	rm -rf $(OUTDIR)

BUILD_NUMBER ?= 0
PROJECT_NAME := snapsys
PROJECT_VERSION := 0.3.$(BUILD_NUMBER)
PACKAGE_VERSION = 1~$(DSC_CODENAME)1
include /usr/share/mkdeb/mkdeb.mk
